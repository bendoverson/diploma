PRIMARY KEY (user_id, recipe_id)

CREATE DATABASE cooking_helper_db;

USE cooking_helper_db;

CREATE TABLE recipes (
	id INTEGER NOT NULL,
	name varchar(128) NOT NULL,
	instructions TEXT NOT NULL,
	photo TEXT NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY (name)
);

CREATE TABLE ingredients (
	id INTEGER NOT NULL,
	name varchar(128) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY (name)
);

CREATE TABLE recipes_ingredients (
	recipe_id INTEGER NOT NULL,
	ingredient_id INTEGER NOT NULL,
	measure TEXT NOT NULL,
	amount TEXT NOT NULL,
	PRIMARY KEY (recipe_id, ingredient_id)
);

CREATE TABLE views (
	user_id INTEGER NOT NULL,
	recipe_id INTEGER NOT NULL,

);

CREATE TABLE marks (
	user_id INTEGER NOT NULL,
	recipe_id INTEGER NOT NULL,
	mark INTEGER NOT NULL,
	PRIMARY KEY (user_id, recipe_id)
);
CREATE TABLE recommendations (
	user_id INTEGER NOT NULL,
	recipe_id INTEGER NOT NULL,
	PRIMARY KEY (user_id, recipe_id)
);

CREATE TABLE IF NOT EXISTS `roles` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` varchar(32) NOT NULL,
    `description` varchar(255) NOT NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `uniq_name` (`name`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

  INSERT INTO `roles` (`id`, `name`, `description`) VALUES(1, 'login', 'Login privileges, granted after account confirmation');

  CREATE TABLE IF NOT EXISTS `roles_users` (
    `user_id` int(10) UNSIGNED NOT NULL,
    `role_id` int(10) UNSIGNED NOT NULL,
    PRIMARY KEY  (`user_id`,`role_id`),
    KEY `fk_role_id` (`role_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  INSERT INTO roles_users (user_id, role_id) VALUES(1, 1);

  CREATE TABLE IF NOT EXISTS `users` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email` varchar(254) NOT NULL,
    `username` varchar(32) NOT NULL DEFAULT '',
    `password` varchar(64) NOT NULL,
    `logins` int(10) UNSIGNED NOT NULL DEFAULT '0',
    `last_login` int(10) UNSIGNED,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `uniq_username` (`username`),
    UNIQUE KEY `uniq_email` (`email`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

  #INSERT INTO `users` (`username`, `email`, `password`) VALUES('admin', 'admin@admin.admin', '9ed6903045238d97ffcf698cdb06c5b83f4d2216748b425d1a4530305280799f');

  CREATE TABLE IF NOT EXISTS `user_tokens` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` int(11) UNSIGNED NOT NULL,
    `user_agent` varchar(40) NOT NULL,
    `token` varchar(40) NOT NULL,
    `created` int(10) UNSIGNED NOT NULL,
    `expires` int(10) UNSIGNED NOT NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `uniq_token` (`token`),
    KEY `fk_user_id` (`user_id`),
    KEY `expires` (`expires`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

  ALTER TABLE `roles_users`
    ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

  ALTER TABLE `user_tokens`
    ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
