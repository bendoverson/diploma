<?php
defined('SYSPATH') or die("NO DIRECT SCRIPT ACCESS");
class Controller_Api extends Controller {

    public function action_getRecipesIngredients() {
        $data               = array();
        $recipes            = Model::factory('Recipes')->get_all()->as_array();
        $ingredients        = Model::factory('Ingredients')->get_all()->as_array();
        $recipesingredients = Model::factory('RecipesIngredients')->get_all()->as_array();
        array_push($data, $recipes);
        array_push($data, $ingredients);
        array_push($data, $recipesingredients);
        $this->response->body(json_encode($data));
    }
    public function action_getUserRecommendedRecipes() {
        $user = Auth::instance()->get_user();
        $data = array();
        if ($user == null) {

            $recipes = Model::factory('Recommendations')->get_top_recipes()->as_array();
            foreach ($recipes as $recipe) {
                $temp              = array();
                $views             = Model::factory('Views')->get_recipe_views($recipe["recipe_id"])->as_array();
                $likes             = Model::factory('Marks')->get_recipe_likes($recipe["recipe_id"])->as_array();
                $dislikes          = Model::factory('Marks')->get_recipe_dislikes($recipe["recipe_id"])->as_array();
                $temp["recipe_id"] = array($recipe["recipe_id"], $views[0]['views'], $likes[0]['likes'], $dislikes[0]['dislikes']);
                array_push($data, $temp);
            }
        } else {
            $recipes = Model::factory('Recommendations')->get_user_recommendations($user->id)->as_array();
            foreach ($recipes as $recipe) {
                $temp              = array();
                $views             = Model::factory('Views')->get_recipe_views($recipe["recipe_id"])->as_array();
                $likes             = Model::factory('Marks')->get_recipe_likes($recipe["recipe_id"])->as_array();
                $dislikes          = Model::factory('Marks')->get_recipe_dislikes($recipe["recipe_id"])->as_array();
                $temp["recipe_id"] = array($recipe["recipe_id"], $views[0]['views'], $likes[0]['likes'], $dislikes[0]['dislikes']);
                array_push($data, $temp);
            }
        }
        $this->response->body(json_encode($data));
    }

    public function action_getRecipesData() {
        $recipes = $this->request->post("recipes");
        $data    = array();
        if ($recipes != null) {
            foreach ($recipes as $recipe) {
                $views         = Model::factory('Views')->get_recipe_views($recipe)->as_array();
                $likes         = Model::factory('Marks')->get_recipe_likes($recipe)->as_array();
                $dislikes      = Model::factory('Marks')->get_recipe_dislikes($recipe)->as_array();
                $data[$recipe] = array($views[0]["views"], $likes[0]["likes"], $dislikes[0]["dislikes"]);
            }
        }
        $this->response->body(json_encode($data));
    }
    public function action_manageMark() {
        $user      = Auth::instance()->get_user();
        $recipe_id = $this->request->post('recipe_id');
        $mark      = $this->request->post('mark');
        $data      = 0;
        if ($user != null) {
            Model::factory('Marks')->remove_mark($user->id, $recipe_id);
            $data = Model::factory('Marks')->add_mark($user->id, $recipe_id, $mark);
        }
        $this->response->body(json_encode($data));
    }

    public function action_getUserData() {
        $user = Auth::instance();
        $data = array();
        if ($user != null) {
            array_push($data, array_merge(Model::factory("Marks")->get_user_likes(1)->as_array(),
                Model::factory("Marks")->get_user_dislikes(1)->as_array()));
            array_push($data, Model::factory("Views")->get_user_views(1)->as_array());
        }
        $this->response->body(json_encode($data));
    }
    public function action_addView() {
        $user      = Auth::instance()->get_user();
        $recipe_id = $this->request->post('recipe_id');
        $data      = 0;
        if ($user != null) {
            $data = Model::factory('Views')->add_view($user->id, $recipe_id);
            $data = Model::factory('Marks')->add_mark($user->id, $recipe_id, -1);
        }
        $this->response->body(json_encode($data));
    }

}
