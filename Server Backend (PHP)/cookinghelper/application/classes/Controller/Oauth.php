<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESSS');

class Controller_Oauth extends Controller {
    public function action_login() {
        $username = $this->request->post('username');
        $password = $this->request->post('password');
        $data     = Auth::instance()->login($username, $password, TRUE);
        if ($data == TRUE) {
            $data          = Auth::instance()->get_user()->as_array();
            $data['token'] = Session::instance()->id();
        }

        $this->response->body(json_encode($data));
    }

    public function action_register() {
        $this->auto_render = FALSE;
        $this->is_ajax     = TRUE;
        header('content-type: application/json');
        try {
            $data = ORM::factory('User')->create_user($this->request->post(), array('username', 'email', 'password'))
                ->add('roles', ORM::factory('role', array('name' => 'login')));
            $this->response->body(json_encode($data));
        } catch (ORM_Validation_Exception $e) {
            $this->response->body(json_encode($e->errors('validation')));
        }
    }
    public function action_logout() {
        Auth::instance()->logout(TRUE);
    }

    public function action_getuser() {
        if (Auth::instance()->logged_in()) {
            $data = Auth::instance()->get_user();
            echo json_encode($data);
        }
    }

    public function action_loggedin() {
        $data = Auth::instance()->logged_in();
        $this->response->body(json_encode($data));
    }
}
?>