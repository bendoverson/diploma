<?php defined('SYSPATH') or die("NO DIRECT SCRIPT ACCESS");

class Controller_Data extends Controller {

    public function cmp($a, $b) {
        if (abs(Arr::get($a, 0)) == abs(Arr::get($b, 0))) {
            return 0;
        }
        return (abs(Arr::get($a, 0)) > abs(Arr::get($b, 0))) ? -1 : 1;
    }

    public function array_cmp($a, $b) {
        if (Arr::get($a, 'recipe_id') == Arr::get($b, 'recipe_id') && Arr::get($a, 'mark') == Arr::get($b, 'mark')) {
            return 0;
        }
        return 1;
    }

    public function tanimoto_array_cmp($a, $b) {
        if ($a['recipe_id'] == $b['recipe_id']) {
            return 0;
        }
        return 1;
    }

    public function action_parseRecipes() {
        include Kohana::find_file('assets', 'simple_html_dom');
        $global_r_id        = 1;
        $global_i_id        = 1;
        $global_ingredients = array();
        $base_url1          = "https://www.edimdoma.ru/retsepty?page=";
        for ($page = 1; $page < 4072; $page++) {
            $url_pool = file_get_html($base_url1 . $page)->find('.card');
            $count    = 1;
            foreach ($url_pool as $url) {
                $url = $url->find('a[!class]', 1);
                if (isset($url)) {
                    set_time_limit(99999999);
                    $recipe_url     = "https://www.edimdoma.ru" . $url->href;
                    $html           = file_get_html($recipe_url);
                    $fn             = $html->find('.recipe-header__name', 0)->plaintext;
                    $duration       = $html->find('.entry-stats__value', 0)->plaintext;
                    $yield          = $html->find('.entry-stats__value', 1)->plaintext;
                    $author         = ($html->find('.recipe-author-block', 0)->find('.person__name', 0)->plaintext) . " [edimdoma.ru]";
                    $photo_filename = "pic" . $global_r_id . ".png";
                    $instructions   = "";
                    $rI             = $html->find('div[itemprop="recipeInstructions"]', 0);
                    if ($rI != null) {
                        $count++;
                        foreach ($rI->find(".plain-text [!class]") as $instruction) {
                            $instructions = $instructions . $instruction->plaintext . "<br>";
                        }
                        $instructions = str_replace("&#188;", "1/4", $instructions);
                        $instructions = str_replace("&#189;", "1/2", $instructions);
                        $instructions = str_replace("&#190;", "3/4", $instructions);
                        $instructions = str_replace("‒", "-", $instructions);
                        $instructions = str_replace("‐", "-", $instructions);
                        $instructions = str_replace("‑", "-", $instructions);
                        $instructions = str_replace("–", "-", $instructions);
                        $instructions = str_replace("—", "-", $instructions);
                        $instructions = str_replace("―", "-", $instructions);
                        $instructions = str_replace("ö", "o", $instructions);
                        $instructions = str_replace("ᵒ", " ", $instructions);
                        try {
                            Model::factory('Recipes')->add_recipe($global_r_id, $fn, $instructions, $photo_filename, $duration, $yield, "Нет информации", $author);
                        } catch (Exception $e) {
                            continue;
                        }
                        $ingredients = array();
                        foreach ($html->find('div[itemprop="ingredients"] .definition-list-table') as $value) {
                            $temp   = array();
                            $i_name = trim($value->find('.definition-list-table__td', 0)->find('span [!class]', 0)->plaintext);
                            $i_val  = trim($value->find('[class="definition-list-table__td definition-list-table__td_value"]', 0)->plaintext);
                            $i_val  = str_replace("&#188;", "1/4", $i_val);
                            $i_val  = str_replace("&#189;", "1/2", $i_val);
                            $i_val  = str_replace("&#190;", "3/4", $i_val);
                            if (isset($global_ingredients[$i_name])) {
                                try {
                                    Model::factory('RecipesIngredients')->add_recipe_ingredient($global_r_id, $global_ingredients[$i_name], "", $i_val);
                                } catch (Exception $e) {
                                }
                            } else {
                                $global_ingredients[$i_name] = $global_i_id;
                                try {
                                    Model::factory('Ingredients')->add_ingredient($global_i_id, $i_name);
                                    Model::factory('RecipesIngredients')->add_recipe_ingredient($global_r_id, $global_i_id, "", $i_val);
                                    $global_i_id = $global_i_id + 1;
                                } catch (Exception $e) {
                                }
                            }
                            array_push($temp, $i_name, $i_val);
                            array_push($ingredients, $temp);
                        }
                        $global_r_id = $global_r_id + 1;
                    } else {
                        continue;
                    }
                }
            }
            echo "TOTALpage: " . $page . "<br>urls_count: " . $count . "<br>";
        }
    }

    public function action_updateUsersRecommendedRecipes() {
        $amount = 15; //количество элементов в списке рекомендаций
        $users  = Model::factory('Users')->get_all()->as_array();
        $users2 = Model::factory('Users')->get_all()->as_array();
        foreach ($users as $user1) {
            set_time_limit(99999999999);
            $ratio_recipes_array  = array();
            $ratio_recipes_array2 = array();
            $result_recipes       = array();
            $id1                  = Arr::get($user1, 'id');
            $likes1               = Model::factory('Marks')->get_user_likes($id1)->as_array();
            $dislikes1            = Model::factory('Marks')->get_user_dislikes($id1)->as_array();
            $views1               = Model::factory('Views')->get_user_views($id1)->as_array();
            foreach ($users2 as $user2) {
                if ($user1 != $user2) {
                    $id2       = Arr::get($user2, 'id');
                    $likes2    = Model::factory('Marks')->get_user_likes($id2)->as_array();
                    $dislikes2 = Model::factory('Marks')->get_user_dislikes($id2)->as_array();
                    $views2    = Model::factory('Views')->get_user_views($id2)->as_array();

                    $l1_l2 = array_uintersect($likes1, $likes2, array(
                        "Controller_Data",
                        "array_cmp",
                    ));
                    $d1_d2 = array_uintersect($dislikes1, $dislikes2, array(
                        "Controller_Data",
                        "array_cmp",
                    ));
                    $l1_d2 = array_uintersect($likes1, $dislikes2, array(
                        "Controller_Data",
                        "array_cmp",
                    ));
                    $d1_l2 = array_uintersect($dislikes1, $likes2, array(
                        "Controller_Data",
                        "array_cmp",
                    ));

                    $count_all = count($likes1) + count($likes2) + count($dislikes1) + count($dislikes2);

                    if ($count_all > 0) {
                        $jaccard_ratio = (count($l1_l2) + count($d1_d2) - count($l1_d2) - count($d1_l2)) / $count_all;
                        if ($jaccard_ratio > 0 && $jaccard_ratio < 1) {
                            $ll_diff = array_udiff($likes2, $likes1, array(
                                "Controller_Data",
                                "array_cmp",
                            ));
                            array_push($ratio_recipes_array, array(
                                $jaccard_ratio,
                                $ll_diff,
                            ));
                        }
                        if ($jaccard_ratio < 0 && $jaccard_ratio > -1) {
                            $ld_diff = array_udiff($dislikes2, $likes1, array(
                                "Controller_Data",
                                "array_cmp",
                            ));
                            array_push($ratio_recipes_array, array(
                                $jaccard_ratio,
                                $ld_diff,
                            ));
                        }
                    }

                }
            }

            $viewsIntersect = array_uintersect($views1, $views2, array(
                "Controller_Data",
                "tanimoto_array_cmp"));

            $viewsDiff = array_udiff($views2, $views1, array(
                "Controller_Data",
                "tanimoto_array_cmp"));

            if ((count($views1) + count($views2) - count($viewsIntersect)) > 0) {
                $tanimoto_coeff = count($viewsIntersect) / ((count($views1) + count($views2) - count($viewsIntersect)));
                if ($tanimoto_coeff > 0) {
                    array_push($ratio_recipes_array2, array($tanimoto_coeff, $viewsDiff));
                }
            }
            usort($ratio_recipes_array, array(
                "Controller_Data",
                "cmp",
            ));
            usort($ratio_recipes_array2, array(
                "Controller_Data",
                "cmp",
            ));

            foreach ($ratio_recipes_array as $array) {
                foreach ($array[1] as $elem) {
                    if (count($result_recipes) < $amount) {
                        if (!in_array($elem['recipe_id'], $result_recipes)) {
                            array_push($result_recipes, $elem['recipe_id']);
                        }
                    } else {
                        break;
                    }
                }
            }
            if (count($result_recipes) < $amount) {
                foreach ($ratio_recipes_array2 as $array) {
                    foreach ($array[1] as $elem) {
                        if (count($result_recipes) < $amount) {
                            if (!in_array($elem['recipe_id'], $result_recipes)) {
                                array_push($result_recipes, $elem['recipe_id']);
                            }
                        } else {
                            break;
                        }
                    }
                }
            }

            if (count($result_recipes) < $amount) {
                $top_recipes = Model::factory('Recommendations')->get_top_recipes()->as_array();
                foreach ($top_recipes as $recipe) {
                    if (count($result_recipes) < $amount) {
                        if (!in_array($recipe['recipe_id'], $result_recipes)) {
                            array_push($result_recipes, $recipe['recipe_id']);
                        }
                    } else {
                        break;
                    }

                }

            }

            if (count($result_recipes) > 0) {
                Model::factory('Recommendations')->delete_user_recommendations($id1);
                Model::factory('Recommendations')->set_user_recommendations($id1, $result_recipes);
            }
        }
    }
}
?>