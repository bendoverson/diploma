<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_RecipesIngredients extends Model {
    protected $TNAME = "recipes_ingredients";
    protected $R_ID  = "recipe_id";
    protected $I_ID  = "ingredient_id";
    protected $M     = "measure";
    protected $A     = "amount";

    public function get_all() {
        return DB::select()
            ->from($this->TNAME)
            ->execute();
    }
    public function add_recipe_ingredient($r, $i, $m, $a) {
    	return DB::insert($this->TNAME, array($this->R_ID,$this->I_ID,$this->M,$this->A))
    			->values(array($r, $i, $m, $a))
    			->execute();
    }
}
