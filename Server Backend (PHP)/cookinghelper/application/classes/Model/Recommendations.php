<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Recommendations extends Model {
    protected $TNAME = "recommendations";
    protected $U_ID  = "user_id";
    protected $R_ID  = "recipe_id";
    public function get_all() {
        return DB::select()
            ->from($this->TNAME)
            ->execute();
    }

    public function get_top_recipes() {
        return DB::select(
            "views.recipe_id",
            array(DB::expr("COUNT(views.user_id)"), "viewscount"),
            array(DB::expr("SUM(marks.mark)"), "markssum"))
            ->from("views")
            ->join("marks")
            ->on("views.recipe_id", '=', "marks.recipe_id")
            ->on("views.user_id", '=', "marks.user_id")
            ->group_by("views.recipe_id")
            ->order_by("viewscount", "desc")
            ->order_by("markssum", "desc")
            ->execute();

    }
    public function get_user_recommendations($id) {
        return DB::select($this->R_ID)
            ->from($this->TNAME)
            ->where($this->U_ID, "=", $id)
            ->execute();
    }
    public function delete_user_recommendations($id) {
        return DB::delete($this->TNAME)
            ->where($this->U_ID, "=", $id)
            ->execute();
    }
    public function set_user_recommendations($id, $recipes) {
        $query = DB::insert($this->TNAME, array($this->U_ID, $this->R_ID));
        foreach ($recipes as $recipe) {
            $query->values(array($id, $recipe));
        }
        return $query->execute();

    }
}
