<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Marks extends Model {
    protected $TNAME = "marks";
    protected $U_ID  = "user_id";
    protected $R_ID  = "recipe_id";
    protected $M     = "mark";

    public function get_user_likes($id) {
        return DB::select($this->R_ID, $this->M)
            ->from($this->TNAME)
            ->where($this->U_ID, "=", $id)
            ->and_where($this->M, "=", 1)
            ->execute();
    }
    public function get_user_dislikes($id) {
        return DB::select($this->R_ID, $this->M)
            ->from($this->TNAME)
            ->where($this->U_ID, "=", $id)
            ->and_where($this->M, "=", -1)
            ->execute();
    }

    public function add_mark($user_id, $recipe_id, $mark) {
        return DB::insert($this->TNAME, array($this->U_ID, $this->R_ID, $this->M))
            ->values(array($user_id, $recipe_id, $mark))
            ->execute();
    }

    public function remove_mark($user_id, $recipe_id) {
        return DB::delete($this->TNAME)
            ->where($this->U_ID, "=", $user_id)
            ->and_where($this->R_ID, "=", $recipe_id)
            ->execute();
    }
    public function get_recipe_likes($id) {
        return DB::select(array(DB::expr("COUNT('{$this->U_ID}')"), "likes"))
            ->from($this->TNAME)
            ->where($this->R_ID, "=", $id)
            ->and_where($this->M, "=", 1)
            ->execute();
    }
    public function get_recipe_dislikes($id) {
        return DB::select(array(DB::expr("COUNT('{$this->U_ID}')"), "dislikes"))
            ->from($this->TNAME)
            ->where($this->R_ID, "=", $id)
            ->and_where($this->M, "=", -1)
            ->execute();
    }
}

?>