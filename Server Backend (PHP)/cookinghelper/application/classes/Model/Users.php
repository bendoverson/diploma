<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Users extends Model {
    protected $TNAME = "users";
    protected $ID    = 'id';
    protected $UNAME = "username";
    protected $EMAIL = "email";
    protected $PASS  = "password";
    public function get_all() {
        return DB::select()->from($this->TNAME)
            ->execute();
    }

    public function delete($id) {
        return DB::delete('users')->where($this->ID, "=", $id)->execute();
    }

}
?>