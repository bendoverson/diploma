<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Recipes extends Model {
    protected $TNAME  = "recipes";
    protected $ID     = "id";
    protected $NAME   = "name";
    protected $INS    = "instructions";
    protected $PHOTO  = "photo";
    protected $DUR    = "duration";
    protected $Y      = "yield";
    protected $NUTR   = "nutrition";
    protected $AUTHOR = "author";
    public function get_all() {
        return DB::select()
            ->from($this->TNAME)
            ->execute();
    }
    public function add_recipe($id, $fn, $instructions, $photo, $duration, $yield, $nutrition, $author) {
        return DB::insert($this->TNAME, array($this->ID, $this->NAME, $this->INS, $this->PHOTO, $this->DUR, $this->Y, $this->NUTR, $this->AUTHOR))
            ->values(array($id, $fn, $instructions, $photo, $duration, $yield, $nutrition, $author))
            ->execute();
    }
}
