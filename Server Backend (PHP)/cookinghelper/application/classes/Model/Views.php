<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Views extends Model {
    protected $TNAME = "views";
    protected $U_ID  = "user_id";
    protected $R_ID  = "recipe_id";

    public function get_all() {
        return DB::select()
            ->from($this->TNAME)
            ->execute();
    }

    public function add_view($user_id, $recipe_id) {
        return DB::insert($this->TNAME, array($this->U_ID, $this->R_ID))
            ->values(array($user_id, $recipe_id))
            ->execute();
    }
    public function get_user_views($u_id) {
        return DB::select()
            ->from($this->TNAME)
            ->where($this->U_ID, "=", $u_id)
            ->execute();
    }
    public function get_recipe_views($r_id) {
        return DB::select(array(DB::expr("COUNT('{$this->U_ID}')"), "views"))
            ->from($this->TNAME)
            ->where($this->R_ID, "=", $r_id)
            ->execute();
    }
}
?>