<?php defined('SYSPATH') or die('NO DIRECT SCRIPT ACCESS');
class Model_Ingredients extends Model {
    protected $TNAME = "ingredients";
    protected $ID    = "id";
    protected $NAME  = "name";

    public function get_all() {
        return DB::select()
            ->from($this->TNAME)
            ->execute();
    }
    public function add_ingredient($id, $name) {
        return DB::insert($this->TNAME, array($this->ID, $this->NAME))
            ->values(array($id, $name))
            ->execute();
    }
}
?>