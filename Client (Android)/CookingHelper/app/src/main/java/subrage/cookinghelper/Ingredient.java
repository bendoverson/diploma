package subrage.cookinghelper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by interstellar on 17.10.2016.
 */
public class Ingredient implements Parcelable {
    //реализовать интерфейс Parcelable
    private int id;
    private String name;
    private String measure;
    private String amount;

    Ingredient(int id, String name) {
        this.id = id;
        this.name = name;
        this.measure = null;
        this.amount = null;
    }

    Ingredient(int id, String name, String measure, String amount) {
        this.id = id;
        this.name = name;
        this.measure = measure;
        this.amount = amount;
    }

    protected Ingredient(Parcel in) {
        id = in.readInt();
        name = in.readString();
        measure = in.readString();
        amount = in.readString();
    }

    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        Ingredient ingredient = (Ingredient) obj;
        return this.id == ingredient.id;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getMeasure() {
        return this.measure;
    }

    public String getAmount() {
        return this.amount;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(measure);
        dest.writeString(amount);
    }
}
