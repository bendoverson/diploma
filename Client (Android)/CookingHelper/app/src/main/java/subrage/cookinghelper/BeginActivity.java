package subrage.cookinghelper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yung mulatto on 02.04.2017.
 */

public class BeginActivity extends AppCompatActivity {
    CookingHelperApp.Service service;
    CookingHelperApp application;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.begin_activity);
        application = (CookingHelperApp) getApplication();
        preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
        final boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
        final CookingHelperApp.Service service = application.getService();
        Call<ResponseBody> isLoggedIn = service.getApi().isLoggedIn();
        boolean skipLogin = preferences.getBoolean(CookingHelperApp.Preferences.SKIPLOGIN, false);
        boolean downloaded = preferences.getBoolean(CookingHelperApp.Preferences.RECIPES_DOWNLOADED, false);
        if (!downloaded) {
            startActivity(new Intent(this, DownloadRecipesActivity.class));
            finish();
        } else {
            if (logged_in) {
                isLoggedIn.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.body().string() == "false") {
                                final CookingHelperApp.User user = application.getUser();
                                if (user != null) {
                                    final Call<ResponseBody> login = service.getApi().logIn(user.getUsername(), user.getPassword());
                                    login.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            if (response.isSuccessful()) {
                                                try {
                                                    String responseValue = response.body().string();
                                                    Log.d("onResponse", responseValue);
                                                    if (Objects.equals(responseValue, "false")) {
                                                        preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
                                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                        startActivity(intent);
                                                    } else {
                                                        preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, true).apply();
                                                        application.getService().getUserData();
                                                        try {
                                                            JSONObject json = new JSONObject(responseValue);
                                                            application.setUser(
                                                                    json.get(CookingHelperApp.Preferences.ID).toString(),
                                                                    json.get(CookingHelperApp.Preferences.EMAIL).toString(),
                                                                    json.get(CookingHelperApp.Preferences.USERNAME).toString(),
                                                                    json.get(CookingHelperApp.Preferences.PASSWORD).toString(),
                                                                    user.getPasswordDec(),
                                                                    json.get(CookingHelperApp.Preferences.LAST_LOGIN).toString());
                                                            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                                                            startActivity(intent);
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } catch (IOException e) {
                                                    Toast.makeText(getApplicationContext(), "Ошибка: " + response.code(), Toast.LENGTH_LONG).show();
                                                    Intent intent = new Intent(BeginActivity.this, MenuActivity.class);
                                                    startActivity(intent);
                                                }
                                            } else {
                                                Intent intent = new Intent(BeginActivity.this, MenuActivity.class);
                                                startActivity(intent);
                                                Toast.makeText(getApplicationContext(), "Ошибка: " + response.code(), Toast.LENGTH_LONG).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                            preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
                                            application.setUser(null);
                                            Intent intent = new Intent(BeginActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                                } else {
                                    preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
                                    Intent intent = new Intent(BeginActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                }

                            } else {
                                Intent intent = new Intent(BeginActivity.this, MenuActivity.class);
                                startActivity(intent);
                            }
                        } catch (IOException e) {
                            if (application.getUser() != null) {
                                Intent intent = new Intent(BeginActivity.this, MenuActivity.class);
                                startActivity(intent);
                            } else {
                                preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
                                Intent intent = new Intent(BeginActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (!(logged_in)) {
                            preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
                            application.setUser(null);
                            Intent intent = new Intent(BeginActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(BeginActivity.this, MenuActivity.class);
                            startActivity(intent);
                        }
                    }
                });
            } else {
                if (skipLogin) {
                    startActivity(new Intent(this, MenuActivity.class));
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                }
            }
            finish();

        }
    }
}
