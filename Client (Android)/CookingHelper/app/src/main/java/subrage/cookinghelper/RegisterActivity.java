package subrage.cookinghelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yung mulatto on 12.04.2017.
 */

public class RegisterActivity extends AppCompatActivity {

    TextView username;
    TextView email;
    TextView password;
    TextView password_confirm;
    CookingHelperApp.Service service;
    CookingHelperApp app;
    TextView registerResponse;

    @Override
    protected void onCreate(Bundle savedStateInstance) {
        super.onCreate(savedStateInstance);
        setContentView(R.layout.register_activity);
        username = (TextView) findViewById(R.id.inputName);
        email = (TextView) findViewById(R.id.inputEmail);
        password = (TextView) findViewById(R.id.inputPassword);
        password_confirm = (TextView) findViewById(R.id.inputPasswordConfrim);
        app = (CookingHelperApp) getApplication();
        service = app.getService();
        registerResponse = (TextView) findViewById(R.id.registerResponse);

    }

    public void onRegisterUserClick(View view) {
        registerResponse.setText("");
        boolean correct = true;
        String message = "";
        if (password.getText().length() < 8) {
            correct &= false;
            message += "Пароль должен содержать минимум 8 символов\n";
        }
        final String finalMessage = message;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                registerResponse.setText(finalMessage);
            }
        });
        if (correct) {
            Call<ResponseBody> regResponse = service.getApi().registerUser(
                    username.getText().toString(),
                    email.getText().toString(),
                    password.getText().toString(),
                    password_confirm.getText().toString());
            regResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        String m = "";
                        boolean correctRequest = true;
                        try {
                            String responseValue = response.body().string();

                            Log.d("responseVal", responseValue);
                            JSONObject json = new JSONObject(responseValue);
//                            if ()){

                            if (!json.isNull("username")) {
                                m += "Такое имя пользователя уже используется\n";
                                correctRequest &= false;
                            }
                            if (!json.isNull("email")) {
                                m += "Такой e-mail уже используется\n";
                                correctRequest &= false;
                            }

                            if (!json.isNull("_external")) {
                                correctRequest &= false;
                                if (!((JSONObject) json.get("_external")).isNull("password_confirm"))
                                    m += "Пароли не совпадают\n";
                            }
                        } catch (Exception ignored) {
                        }
                        final String finalM = m;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                registerResponse.setText(finalM);
                            }
                        });
                        if (correctRequest) {
                            finish();
                        }
                    } else {
                        try {
                            Log.d("onResponse: ", response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("REGISTER FAILURE", t.getMessage());
                }
            });
        } else

        {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        }
    }


}
