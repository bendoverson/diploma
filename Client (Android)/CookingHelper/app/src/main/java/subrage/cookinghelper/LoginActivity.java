package subrage.cookinghelper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    TextView username;
    TextView password;
    CookingHelperApp.Service service;
    CookingHelperApp app;
    SharedPreferences preferences;
    TextView loginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        username = (TextView) findViewById(R.id.inputLoginName);
        password = (TextView) findViewById(R.id.inputLoginPassword);
        app = (CookingHelperApp) getApplication();
        service = app.getService();
        loginResponse = (TextView) findViewById(R.id.loginResponse);
        preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
    }

    public void onLoginClick(View view) {
        loginResponse.setText("");
        final Call<ResponseBody> login = service.getApi().logIn(username.getText().toString(), password.getText().toString());
        login.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String responseValue = response.body().string();
                        Log.d("onResponse: ", responseValue);
                        if (responseValue.equals("false")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loginResponse.setText("Неверный логин или пароль");
                                }
                            });
                            preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();

                        } else {
                            app.getService().getUserData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loginResponse.setText("Вы вошли в систему");
                                }
                            });
                            preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, true).apply();
                            try {
                                Log.d("json", responseValue);
                                JSONObject json = new JSONObject(responseValue);
                                app.setUser(
                                        json.get(CookingHelperApp.Preferences.ID).toString(),
                                        json.get(CookingHelperApp.Preferences.EMAIL).toString(),
                                        json.get(CookingHelperApp.Preferences.USERNAME).toString(),
                                        json.get(CookingHelperApp.Preferences.PASSWORD).toString(),
                                        password.getText().toString(),
                                        json.get(CookingHelperApp.Preferences.LAST_LOGIN).toString());
                                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                                preferences.edit().putBoolean(CookingHelperApp.Preferences.SKIPLOGIN, false).apply();
                                boolean menuActivityRequest = getIntent().getBooleanExtra("menuActivityRequest", false);
                                if (menuActivityRequest) {
                                    setResult(1);
                                } else {
                                    startActivity(intent);
                                }
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    final int code = response.code();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loginResponse.setText("Ошибка: " + code);
                        }
                    });

                }
                // Log.d("onLoginResponseSuccess", String.valueOf(response.code()));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, final Throwable t) {
                switch (t.getMessage()) {
                    case "No route to host":

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginResponse.setText("Невозможно соединиться с сервером");
                            }
                        });
                        break;
                    case "connect timed out":
                    case "timeout":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginResponse.setText("Время ожидания истекло");
                            }
                        });
                        break;
                    default:
                        final String p = t.getMessage();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loginResponse.setText(p);
                            }
                        });
                        break;
                }

            }
        });
    }

    public void onSkipLoginClick(View view) {
        preferences.edit().putBoolean(CookingHelperApp.Preferences.SKIPLOGIN, true).apply();
        preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();
        Intent intent = new Intent(this, MenuActivity.class);
        boolean menuActivityRequest = getIntent().getBooleanExtra("menuActivityRequest", false);
        if (menuActivityRequest) {
            setResult(0);
        } else {
            startActivity(intent);
        }
        finish();
    }

    public void onRegisterClick(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, 1);
    }

}
