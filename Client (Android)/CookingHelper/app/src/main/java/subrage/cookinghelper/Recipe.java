package subrage.cookinghelper;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by interstellar on 26.10.2016.
 */
public class Recipe implements Parcelable {
    private int id;
    private String name;
    private String instructions;
    private String photo;
    private String duration;
    private String yield;
    private String nutrition;
    private String author;
    private int views;
    private int likes;
    private int dislikes;
    private ArrayList<Ingredient> ingredients;
    private ArrayList<Ingredient> missingIngredients;


    Recipe(int id, String name, String instructions, String photo, String duration, String yield, String nutrition, String author, ArrayList<Ingredient> ingredients, ArrayList<Ingredient> missingIngredients) {
        this.id = id;
        this.name = name;
        this.instructions = instructions;
        this.photo = photo;
        this.duration = duration;
        this.yield = yield;
        this.nutrition = nutrition;
        this.author = author;
        this.views = -1;
        this.likes = -1;
        this.dislikes = -1;
        this.ingredients = ingredients;
        this.missingIngredients = missingIngredients;
    }

    Recipe(Parcel in) {
        id = in.readInt();
        name = in.readString();
        instructions = in.readString();
        photo = in.readString();
        duration = in.readString();
        yield = in.readString();
        nutrition = in.readString();
        author = in.readString();
        views = in.readInt();
        likes = in.readInt();
        dislikes = in.readInt();
        ingredients = new ArrayList<>();
        in.readTypedList(ingredients, Ingredient.CREATOR);
        missingIngredients = new ArrayList<>();
        in.readTypedList(missingIngredients, Ingredient.CREATOR);
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getViews() {
        return this.views;
    }

    public int getLikes() {
        return this.likes;
    }

    public int getDislikes() {
        return this.dislikes;
    }

    public ArrayList<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public ArrayList<Ingredient> getMissingIngredients() {
        return this.missingIngredients;
    }

    public String getInstructions() {
        return this.instructions;
    }

    public String getPhotoSrc() {
        return this.photo;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void setDisliked(int dislikes) {
        this.dislikes = dislikes;
    }

    public void setIngredients(ArrayList<Ingredient> list) {
        this.ingredients = new ArrayList<>();
        this.ingredients.addAll(list);
    }

    public void setMissingIngredients(ArrayList<Ingredient> list) {
        this.missingIngredients = new ArrayList<>();
        this.missingIngredients.addAll(list);
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(instructions);
        dest.writeString(photo);
        dest.writeString(duration);
        dest.writeString(yield);
        dest.writeString(nutrition);
        dest.writeString(author);
        dest.writeInt(views);
        dest.writeInt(likes);
        dest.writeInt(dislikes);
        dest.writeTypedList(ingredients);
        dest.writeTypedList(missingIngredients);
    }

    public String getDuration() {
        return duration;
    }

    public String getNutrition() {
        return nutrition;
    }

    public String getYield() {
        return yield;
    }

    public String getAuthor() {
        return author;
    }
}
