package subrage.cookinghelper;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by interstellar on 23.10.2016.
 */
public class IngredientsListAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private ArrayList<Ingredient> ingredients;
    private ArrayList<Ingredient> ingredientsFiltered;
    private LayoutInflater inflater;

    IngredientsListAdapter(Context context, ArrayList<Ingredient> list){
        this.context = context;
        ingredients  = list;
        ingredientsFiltered = ingredients;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return ingredientsFiltered.size();
    }

    @Override
    public Ingredient getItem(int position) {
        return ingredientsFiltered.get(position);
    }
    @Override
    public long getItemId(int position) {
        return ingredientsFiltered.get(position).getId();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = inflater.inflate(R.layout.dropdown_ingredient, parent, false);
        }
        Ingredient ingredient = getItem(position);
        TextView field = (TextView) view.findViewById(R.id.ingredientName);
        field.setText(ingredient.getName());
        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.count = ingredients.size();
                    results.values = ingredients;
                } else {
                    ArrayList<Ingredient> resultsData = new ArrayList<>();
                    String searchStr = constraint.toString().trim().toUpperCase();
                    for (Ingredient s : ingredients) {
                        if (s.getName().toUpperCase().contains(searchStr)) resultsData.add(s);
                    }
                    results.values = resultsData;
                    results.count = resultsData.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ingredientsFiltered = (ArrayList<Ingredient>) results.values;
                notifyDataSetChanged();
            }
        };
    }


}
