package subrage.cookinghelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by interstellar on 16.05.2017.
 */

public class DownloadRecipesActivity extends AppCompatActivity {
    CookingHelperApp app;
    SharedPreferences preferences;
    Database database;
    GifImageView gif;
    TextView loadingMessage;
    int pausestate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pausestate = 0;
        setContentView(R.layout.download_recipes_activity);
        gif = (GifImageView) findViewById(R.id.loading2);
        gif.setGifImageResource(R.drawable.loading);
        app = (CookingHelperApp) getApplication();
        loadingMessage = (TextView) findViewById(R.id.loadingMessage);
        preferences = app.getSharedPreferences(String.valueOf(R.string.preferences), MODE_PRIVATE);
        CookingHelperApp.Service service = app.getService();
        CookingHelperAPI api = service.getApi();
        database = new Database(this);
        Call<ResponseBody> call = api.getRecipesIngredients();
        loadingMessage.setText("Соединение с сервером");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingMessage.setText("Соединение установлено");
                                pausestate = 1;
                            }
                        });

                        database.removePreviousData();
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = (JsonArray) parser.parse(response.body().string());
                        JsonArray recipes = jsonArray.get(0).getAsJsonArray();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingMessage.setText("Загрузка рецептов");
                                pausestate = 2;
                                gif.setGifImageResource(R.drawable.ringalt3);
                            }
                        });

                        for (JsonElement recipe : recipes) {
                            JsonObject obj = recipe.getAsJsonObject();

                            String id = String.valueOf(obj.get("id"));
                            id = id.substring(1, id.length() - 1);

                            String name = String.valueOf(obj.get("name"));
                            name = name.substring(1, name.length() - 1);

                            String instructions = String.valueOf(obj.get("instructions"));
                            instructions = instructions.substring(1, instructions.length() - 1);
                            instructions = instructions.replace("<br>", "\n\n");

                            String photo = String.valueOf(obj.get("photo"));
                            photo = photo.substring(1, photo.length() - 1);

                            String duration = String.valueOf(obj.get("duration"));
                            duration = duration.substring(1, duration.length() - 1);

                            String yield = String.valueOf(obj.get("yield"));
                            yield = yield.substring(1, yield.length() - 1);

                            String nutrition = String.valueOf(obj.get("nutrition"));
                            nutrition = nutrition.substring(1, nutrition.length() - 1);

                            String author = String.valueOf(obj.get("author"));
                            author = author.substring(1, author.length() - 1);

//                            Log.d("recipe", id + " " + name + " " + photo + " " + duration + " " + yield + " " + nutrition + " " + author);

                            database.addRecipe(id, name, instructions, photo, duration, yield, nutrition, author);
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingMessage.setText("Загрузка ингредиентов");
                                pausestate = 3;
                                gif.setGifImageResource(R.drawable.ringalt2);
                            }
                        });
                        JsonArray ingredients = jsonArray.get(1).getAsJsonArray();
                        for (JsonElement ingredient : ingredients) {
                            JsonObject obj = ingredient.getAsJsonObject();
                            String id = String.valueOf(obj.get("id"));
                            id = id.substring(1, id.length() - 1);
                            String name = String.valueOf(obj.get("name"));
                            name = name.substring(1, name.length() - 1);
//                            Log.d("ing", id + " " + name);
                            database.addIngredient(id, name);
                        }
                        JsonArray recipes_ingredients = jsonArray.get(2).getAsJsonArray();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingMessage.setText("Загрузка вспомогательной информации");
                                pausestate = 4;
                                gif.setGifImageResource(R.drawable.ringalt1);
                            }
                        });
                        for (JsonElement rec_ing : recipes_ingredients) {
                            JsonObject obj = rec_ing.getAsJsonObject();
                            String recipe_id = String.valueOf(obj.get("recipe_id"));
                            recipe_id = recipe_id.substring(1, recipe_id.length() - 1);
                            String ingredient_id = String.valueOf(obj.get("ingredient_id"));
                            ingredient_id = ingredient_id.substring(1, ingredient_id.length() - 1);
                            String measure = String.valueOf(obj.get("measure"));
                            measure = measure.substring(1, measure.length() - 1);
                            String amount = String.valueOf(obj.get("amount"));
                            amount = amount.substring(1, amount.length() - 1);
//                            Log.d("recipeing", recipe_id + " " + ingredient_id + " " + measure);
                            database.addRecipeIngredient(recipe_id, ingredient_id, measure, amount);
                        }
                        preferences.edit().putBoolean(CookingHelperApp.Preferences.RECIPES_DOWNLOADED, true).apply();
                        startActivity(new Intent(DownloadRecipesActivity.this, BeginActivity.class));
                        finish();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Невозможно соединиться с сервером", Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (pausestate) {
            case 4:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingMessage.setText("Загрузка вспомогательной информации");
                        pausestate = 4;
                        gif.setGifImageResource(R.drawable.ringalt1);
                    }
                });
                break;
            case 3:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingMessage.setText("Загрузка ингредиентов");
                        pausestate = 3;
                        gif.setGifImageResource(R.drawable.ringalt2);
                    }
                });
                break;
            case 2:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingMessage.setText("Загрузка рецептов");
                        pausestate = 2;
                        gif.setGifImageResource(R.drawable.ringalt3);
                    }
                });
                break;
            case 1:
                Toast.makeText(getApplicationContext(), "Ошибка\nПопробуйте снова", Toast.LENGTH_LONG).show();
                preferences.edit().putBoolean(CookingHelperApp.Preferences.RECIPES_DOWNLOADED, false).apply();
                finish();
                break;

        }

    }
}
