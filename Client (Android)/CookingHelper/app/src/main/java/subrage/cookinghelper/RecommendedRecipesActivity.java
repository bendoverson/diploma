package subrage.cookinghelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.ArrayRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yung mulatto on 04.04.2017.
 */

public class RecommendedRecipesActivity extends AppCompatActivity {
    private ListView recipesList;
    private Database database;
    private LinearLayout imageLayout;
    private TextView notFoundMessage;
    private Intent newIntent;
    private HashMap<Integer, Integer> userMarks;
    private CookingHelperApp app;
    private SharedPreferences preferences;
    ArrayList<Recipe> recipes;
    RecipesListAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_recipes_activity);
        app = (CookingHelperApp) getApplication();
        preferences = app.getSharedPreferences(String.valueOf(R.string.preferences), MODE_PRIVATE);
        final boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
        recipesList = (ListView) findViewById(R.id.resultRecipesList);
        imageLayout = (LinearLayout) findViewById(R.id.imageLayout);
        GifImageView gifImageView = (GifImageView) findViewById(R.id.loading);
        gifImageView.setGifImageResource(R.drawable.ringalt3);
        notFoundMessage = (TextView) findViewById(R.id.notFound);
        database = new Database(this);
        userMarks = new HashMap<>();
        class GetMarks extends AsyncTask<Void, Void, HashMap<Integer, Integer>> {

            @Override
            protected HashMap<Integer, Integer> doInBackground(Void... params) {
                HashMap<Integer, Integer> result = new HashMap<>();
                boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
                if (logged_in) {
                    String user_id = preferences.getString(CookingHelperApp.Preferences.ID, null);
                    if (user_id != null) {
                        Cursor marks = database.getUserMarks(((CookingHelperApp) getApplication()).getUser().getId());
                        while (marks.moveToNext()) {
                            result.put(marks.getInt(0), marks.getInt(1));
                        }
                    }
                }
                return result;
            }

            @Override
            protected void onPostExecute(HashMap<Integer, Integer> result) {
                userMarks.putAll(result);
                final Call<ResponseBody> recommendations = app.getService().getApi().getUserRecommendedRecipes();
                recommendations.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (logged_in && app.getUser() != null) {
                            database.removePreviousRecommendations(app.getUser().getId());
                        } else {
                            database.removePreviousRecommendations("-1");
                        }

                        JsonParser parser = new JsonParser();
                        recipes = new ArrayList<>();
                        try {
                            JsonArray array = parser.parse(response.body().string()).getAsJsonArray();
                            for (JsonElement element : array) {
                                JsonArray obj = element.getAsJsonObject().get("recipe_id").getAsJsonArray();
                                Cursor cursor = database.getRecipe(obj.get(0).getAsString());
                                cursor.moveToNext();
                                Recipe recipe = new Recipe(
                                        cursor.getInt(0),
                                        cursor.getString(1),
                                        cursor.getString(2),
                                        cursor.getString(3),
                                        cursor.getString(4),
                                        cursor.getString(5),
                                        cursor.getString(6),
                                        cursor.getString(7),
                                        null,
                                        new ArrayList<Ingredient>());
                                recipe.setViews(obj.get(1).getAsInt());
                                recipe.setLikes(obj.get(2).getAsInt());
                                recipe.setDisliked(obj.get(3).getAsInt());
                                cursor = database.getRecipeIngredients(recipe.getId());
                                if (logged_in && app.getUser() != null) {
                                    database.addRecommendation(app.getUser().getId(), recipe.getId() + "");
                                } else {
                                    database.addRecommendation("-1", recipe.getId() + "");
                                }
                                ArrayList<Ingredient> recipeIngredients = new ArrayList<>();
                                while (cursor.moveToNext()) {
                                    int _id = cursor.getInt(0);
                                    String _name = cursor.getString(1);
                                    String _measure = cursor.getString(2);
                                    String _amount = cursor.getString(3);
                                    recipeIngredients.add(new Ingredient(_id, _name, _measure, _amount));
                                }
                                recipe.setIngredients(recipeIngredients);
                                recipes.add(recipe);
                                database.addRecipeData(obj.get(0).getAsInt(),
                                        obj.get(1).getAsInt(),
                                        obj.get(2).getAsInt(),
                                        obj.get(3).getAsInt());
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    imageLayout.setVisibility(View.GONE);
                                    if (recipes == null || recipes.isEmpty()) {
                                        notFoundMessage.setVisibility(View.VISIBLE);
                                    } else {
                                        recipesList.setVisibility(View.VISIBLE);
                                        adapter = new RecipesListAdapter(RecommendedRecipesActivity.this, recipes, userMarks, true);
                                        recipesList.setAdapter(adapter);
                                        recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                Recipe recipe = (Recipe) adapter.getItem(position);
                                                newIntent = new Intent(RecommendedRecipesActivity.this, RecipeActivity.class);
                                                newIntent.putExtra("Recipe", recipe);
                                                if (userMarks.containsKey(recipe.getId())) {
                                                    int mark = userMarks.get(recipe.getId());
                                                    newIntent.putExtra("Mark", mark);
                                                }
                                                newIntent.putExtra("Position", position);
                                                startActivityForResult(newIntent, 0);
                                            }
                                        });

                                    }
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Невозможно обновить рекомендации", Toast.LENGTH_LONG).show();
                            }
                        });
                        Cursor recommendationsCursor;
                        recipes = new ArrayList<>();
                        if (logged_in && app.getUser() != null)
                            recommendationsCursor = database.getRecommendations(app.getUser().getId());
                        else
                            recommendationsCursor = database.getRecommendations("-1");
                        while (recommendationsCursor.moveToNext()) {

                            Cursor cursor = database.getRecipe(recommendationsCursor.getString(0));
                            Log.d("recscursor", recommendationsCursor.getString(0));
                            if (cursor.moveToNext()) {
                                Recipe recipe = new Recipe(
                                        cursor.getInt(0),
                                        cursor.getString(1),
                                        cursor.getString(2),
                                        cursor.getString(3),
                                        cursor.getString(4),
                                        cursor.getString(5),
                                        cursor.getString(6),
                                        cursor.getString(7),
                                        null,
                                        new ArrayList<Ingredient>());

                                Cursor rLikes = database.getRecipeMarks(recipe.getId());
                                if (rLikes.moveToNext()) {
                                    recipe.setViews(rLikes.getInt(1));
                                }
                                Cursor rMarks = database.getRecipeMarks(recipe.getId());
                                if (rMarks.moveToNext()) {
                                    recipe.setLikes(rMarks.getInt(1));
                                    recipe.setDisliked(rMarks.getInt(2));
                                }
                                ArrayList<Ingredient> recipeIngredients = new ArrayList<>();
                                Cursor ingredientsCursor = database.getRecipeIngredients(recipe.getId());

                                while (ingredientsCursor.moveToNext()) {
                                    int _id = ingredientsCursor.getInt(0);
                                    String _name = ingredientsCursor.getString(1);
                                    String _measure = ingredientsCursor.getString(2);
                                    String _amount = ingredientsCursor.getString(3);
                                    recipeIngredients.add(new Ingredient(_id, _name, _measure, _amount));
                                }
                                recipe.setIngredients(recipeIngredients);
                                recipes.add(recipe);
                            }
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageLayout.setVisibility(View.GONE);
                                if (recipes == null || recipes.isEmpty()) {
                                    notFoundMessage.setVisibility(View.VISIBLE);
                                } else {
                                    recipesList.setVisibility(View.VISIBLE);
                                    adapter = new RecipesListAdapter(RecommendedRecipesActivity.this, recipes, userMarks, true);
                                    recipesList.setAdapter(adapter);
                                    recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            Recipe recipe = (Recipe) adapter.getItem(position);
                                            newIntent = new Intent(RecommendedRecipesActivity.this, RecipeActivity.class);
                                            newIntent.putExtra("Recipe", recipe);
                                            if (userMarks.containsKey(recipe.getId())) {
                                                int mark = userMarks.get(recipe.getId());
                                                newIntent.putExtra("Mark", mark);
                                            }
                                            newIntent.putExtra("Position", position);
                                            startActivityForResult(newIntent, 0);
                                        }
                                    });

                                }
                            }
                        });
                    }
                });
            }
        }
        new GetMarks().execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            int position = data.getIntExtra("Position", -1);
            if (position != -1) {
                Recipe recipe = (Recipe) adapter.getItem(position);
                switch (resultCode) {
                    case 0:
                        adapter.removeMarkedRecipe(recipe.getId());
                        break;
                    default:
                        adapter.addMarkedRecipe(recipe.getId(), resultCode);
                        break;
                }
                adapter.notifyDataSetChanged();

            }
        }
    }
}

