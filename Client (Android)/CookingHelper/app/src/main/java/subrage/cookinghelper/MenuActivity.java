package subrage.cookinghelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
    CookingHelperApp application;
    CookingHelperApp.Service service;
    Button login;
    Button logout;
    Button register;
    SharedPreferences preferences;

    Button likedRecipes;
    Button watchedRecipes;
    TextView userLogin;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);
        login = (Button) findViewById(R.id.menuLoginButton);
        logout = (Button) findViewById(R.id.menuLogoutButton);
        register = (Button) findViewById(R.id.menuRegisterButton);
        likedRecipes = (Button) findViewById(R.id.likedRecipesButton);
        watchedRecipes = (Button) findViewById(R.id.watchedRecipesButton);
        userLogin = (TextView) findViewById(R.id.user_login);
        application = (CookingHelperApp) getApplication();
        service = application.getService();
        preferences = application.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);

        boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
        if (logged_in) {
            login.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            userLogin.setText(application.getUser().getUsername());
        } else {
            logout.setVisibility(View.GONE);
            likedRecipes.setVisibility(View.GONE);
            watchedRecipes.setVisibility(View.GONE);
            userLogin.setVisibility(View.GONE);
        }

//        Call<ResponseBody> isLoggedIn = service.getApi().isLoggedIn();
//        isLoggedIn.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
////                    Log.d("onBeginIsLoggedIn", response.errorBody().string());
//                    Log.d("onBeginIsLoggedIn", String.valueOf(response.code()));
//                    Log.d("onBeginIsLoggedIn", response.body().string());
//                } catch (IOException e) {
//                    Log.d("onBeginIsLoggedIn", e.getMessage());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d("onBeginIsLoggedIn", t.getMessage());
//            }
//        });
    }

    protected void onStartCookingClick(View view) {
        Intent intent = new Intent(this, SelectIngredientsActivity.class);
        startActivity(intent);
    }

    protected void onRecommendationsClick(View view) {
        Intent intent = new Intent(this, RecommendedRecipesActivity.class);
        startActivity(intent);
    }

    protected void onLikedRecipesClick(View view) {
        Intent intent = new Intent(this, LikedRecipesActivity.class);
        startActivity(intent);
    }

    protected void onWatchedRecipesClick(View view) {
        Intent intent = new Intent(this, WatchedRecipesActivity.class);
        startActivity(intent);
    }

    protected void onRegisterButtonClick(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    protected void onLoginButtonClick(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("menuActivityRequest", true);
        startActivityForResult(intent, 1);
    }

    protected void onLogoutButtonClick(View view) {
        Call<ResponseBody> logoutReq = application.getService().getApi().logout();

        logoutReq.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                application.setUser(null);
                preferences.edit().putBoolean(CookingHelperApp.Preferences.LOGGED_IN, false).apply();;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logout.setVisibility(View.GONE);
                        login.setVisibility(View.VISIBLE);
                        register.setVisibility(View.VISIBLE);
                        watchedRecipes.setVisibility(View.GONE);
                        likedRecipes.setVisibility(View.GONE);
                        userLogin.setVisibility(View.GONE);

                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            login.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            logout.setVisibility(View.VISIBLE);
            likedRecipes.setVisibility(View.VISIBLE);
            watchedRecipes.setVisibility(View.VISIBLE);
            userLogin.setText(application.getUser().getUsername());
            userLogin.setVisibility(View.VISIBLE);
        } else {
            logout.setVisibility(View.GONE);
            likedRecipes.setVisibility(View.GONE);
            watchedRecipes.setVisibility(View.GONE);
            userLogin.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);
            register.setVisibility(View.VISIBLE);
        }


    }
}
