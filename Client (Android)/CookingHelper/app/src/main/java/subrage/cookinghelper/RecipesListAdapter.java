package subrage.cookinghelper;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by interstellar on 04.03.2017.
 */
public class RecipesListAdapter extends BaseAdapter {
    Context context;
    ArrayList<Recipe> recipes;
    HashMap<Integer, Integer> marks;
    private LayoutInflater inflater;

    Comparator<Recipe> asc = new Comparator<Recipe>() {
        @Override
        public int compare(Recipe o1, Recipe o2) {
            if (o1.getMissingIngredients() != null && o2.getMissingIngredients() != null) {
                int val1 = o2.getMissingIngredients().size() - o1.getMissingIngredients().size();
                if (val1 > 0 || val1 < 0) return -val1;
                else {
                    int val2 = (o2.getLikes() - o2.getDislikes()) - (o1.getLikes() - o1.getDislikes());
                    if (val2 > 0 || val2 < 0) return val2;
                    else return o2.getViews() - o1.getViews();
                }
            } else {
                int val2 = (o2.getLikes() - o2.getDislikes()) - (o1.getLikes() - o1.getDislikes());
                if (val2 > 0 || val2 < 0) return val2;
                else return o2.getViews() - o1.getViews();
            }
        }
    };


    RecipesListAdapter(Context context, ArrayList<Recipe> recipes, HashMap<Integer, Integer> marks) {
        this.context = context;
        this.recipes = recipes;
        this.marks = marks;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (this.recipes != null) {
            Collections.sort(this.recipes, asc);
        }
    }

    RecipesListAdapter(Context context, ArrayList<Recipe> recipes, HashMap<Integer, Integer> marks, boolean sort) {
        this.context = context;
        this.recipes = recipes;
        this.marks = marks;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (sort) {
            if (this.recipes != null) {
                Collections.sort(this.recipes, asc);
            }
        }
    }

    public void addMarkedRecipe(Integer recipe_id, Integer mark) {
        if (!(this.marks.containsKey(recipe_id)))
            this.marks.put(recipe_id, mark);
        else {
            this.marks.remove(recipe_id);
            this.marks.put(recipe_id, mark);
        }
    }

    public void removeMarkedRecipe(Integer recipe_id) {
        if (this.marks.containsKey(recipe_id))
            this.marks.remove(recipe_id);
    }

    @Override
    public int getCount() {
        return recipes.size();
    }

    @Override
    public Object getItem(int position) {
        return recipes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return recipes.get(position).getId();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.recipe, parent, false);
        }
        Recipe recipe = (Recipe) getItem(position);
        TextView textName = (TextView) view.findViewById(R.id.recipeName);
        TextView textViews = (TextView) view.findViewById(R.id.recipeViews);
        TextView textLikes = (TextView) view.findViewById(R.id.recipeLikes);
        TextView textDislikes = (TextView) view.findViewById(R.id.recipeDislikes);
        TextView textIngredients = (TextView) view.findViewById(R.id.recipeIngredients);
        CheckBox checkbox = (CheckBox) view.findViewById(R.id.recipeCheckbox);
        ColorStateList liked = ContextCompat.getColorStateList(context, R.color.colorPrimary);
        ColorStateList disliked = ContextCompat.getColorStateList(context, R.color.background4);
        ColorStateList disabled = ContextCompat.getColorStateList(context, R.color.disabled);

        textName.setText(recipe.getName());
        if (String.valueOf(recipe.getViews()) == "-1") {
            textViews.setText("???");
        } else {
            textViews.setText(String.valueOf(recipe.getViews()));
        }
        if (String.valueOf(recipe.getLikes()) == "-1") {
            textLikes.setText("???");
        } else {
            textLikes.setText(String.valueOf(recipe.getLikes()));
        }
        if (String.valueOf(recipe.getDislikes()) == "-1") {
            textDislikes.setText("???");
        } else {
            textDislikes.setText(String.valueOf(recipe.getDislikes()));
        }
        if (recipe.getMissingIngredients().isEmpty() || recipe.getMissingIngredients() == null) {
            textIngredients.setVisibility(View.GONE);
        } else {
            ArrayList<Ingredient> missingIngredients = recipe.getMissingIngredients();
            String query = "Отсутствует: ";
            if (missingIngredients.size() > 1)
                query = "Отсутствуют: ";
            for (int i = 0; i < missingIngredients.size(); i++) {
                query += missingIngredients.get(i).getName();
                if (i < missingIngredients.size() - 1) {
                    query += ", ";
                }
            }
            textIngredients.setText(query);
        }
        checkbox.setChecked(false);
        checkbox.setButtonTintList(disabled);
        if (marks.containsKey(recipe.getId())) {
            int mark = marks.get(recipe.getId());
            switch (mark) {
                case -1:
                    checkbox.setChecked(true);
                    checkbox.setButtonTintList(disliked);
                    break;
                case 1:
                    checkbox.setChecked(true);
                    checkbox.setButtonTintList(liked);
                    break;
                default:
                    checkbox.setChecked(false);
                    checkbox.setButtonTintList(disabled);
                    break;
            }
        }
        return view;
    }

}
