package subrage.cookinghelper;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Database extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "recipes.db";

    public void addRecipe(String id, String name, String instructions, String photo, String duration, String yield, String nutrition, String author) {
        ContentValues values = new ContentValues();
        values.put(Recipes.ID, id);
        values.put(Recipes.NAME, name);
        values.put(Recipes.INSTRUCTIONS, instructions);
        values.put(Recipes.PHOTO, photo);
        values.put(Recipes.DURATION, duration);
        values.put(Recipes.YIELD, yield);
        values.put(Recipes.NUTRITION, nutrition);
        values.put(Recipes.AUTHOR, author);
        getWritableDatabase().insert(Recipes.TABLENAME, null, values);
    }

    public void addIngredient(String id, String name) {
        ContentValues values = new ContentValues();
        values.put(Ingredients.ID, id);
        values.put(Ingredients.NAME, name);
        getWritableDatabase().insert(Ingredients.TABLENAME, null, values);
    }

    public void addRecipeIngredient(String recipe_id, String ingredient_id, String measure, String amount) {
        ContentValues values = new ContentValues();
        values.put(RecipesIngredients.RECIPE_ID, recipe_id);
        values.put(RecipesIngredients.INGREDIENT_ID, ingredient_id);
        values.put(RecipesIngredients.MEASURE, measure);
        values.put(RecipesIngredients.AMOUNT, amount);
        getWritableDatabase().insert(RecipesIngredients.TABLENAME, null, values);
    }

    public void addRecommendation(String user_id, String recipe_id) {
        ContentValues values = new ContentValues();
        values.put(Recommendations.USER_ID, user_id);
        values.put(Recommendations.RECIPE_ID, recipe_id);
        getWritableDatabase().insert(Recommendations.TABLENAME, null, values);
    }

    public Cursor getRecommendations(String user_id) {
        String query = "SELECT " + Recommendations.RECIPE_ID +
                " FROM " + Recommendations.TABLENAME +
                " WHERE " + Recommendations.USER_ID + " = " + user_id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getRecipe(String id) {
        String query = "SELECT * FROM " + Recipes.TABLENAME + " WHERE " + Recipes.ID + " = " + id;
        return getReadableDatabase().rawQuery(query, null);

    }

    public void removePreviousRecommendations(String user_id) {
        getWritableDatabase().delete(Recommendations.TABLENAME, Recommendations.USER_ID + " = " + user_id, null);
    }

    public Cursor getAllIngredients() {
        return getReadableDatabase().query(Ingredients.TABLENAME, null, null, null, null, null, null);
    }

    public Cursor getAllRecipes() {
        return getReadableDatabase().query(Recipes.TABLENAME, null, null, null, null, null, null);
    }


    public Cursor getRecipeViews(int id) {
        String query = "SELECT * FROM " + Views.TABLENAME + " WHERE " + Views.RECIPE_ID + " = " + id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getUserMarks(String user_id) {
        String query = "SELECT " + UserMarks.RECIPE_ID + "," + UserMarks.MARK +
                " FROM " + UserMarks.TABLENAME +
                " WHERE " + UserMarks.USER_ID + " = " + user_id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getUserLikes(String user_id) {
        String query = "SELECT " + UserMarks.RECIPE_ID + "," + UserMarks.MARK +
                " FROM " + UserMarks.TABLENAME +
                " WHERE " + UserMarks.USER_ID + " = " + user_id + " AND " + UserMarks.MARK + " = " + 1;
        return getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getUserViews(String user_id) {
        String query = "SELECT " + UserViews.RECIPE_ID + " FROM " + UserViews.TABLENAME + " WHERE " + UserViews.USER_ID + " = " + user_id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public Cursor getRecipeMarks(int id) {
        String query = "SELECT * FROM " + Marks.TABLENAME + " WHERE " + Marks.RECIPE_ID + " = " + id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public void removePreviousData() {
        getWritableDatabase().delete(Recipes.TABLENAME, null, null);
        getWritableDatabase().delete(Ingredients.TABLENAME, null, null);
        getWritableDatabase().delete(RecipesIngredients.TABLENAME, null, null);
    }

    public Cursor getRecipeIngredients(int id) {
        String query = "SELECT " +
                Ingredients.TABLENAME + "." + Ingredients.ID + ", " +
                Ingredients.TABLENAME + "." + Ingredients.NAME + ", " +
                RecipesIngredients.TABLENAME + "." + RecipesIngredients.MEASURE + ", " +
                RecipesIngredients.TABLENAME + "." + RecipesIngredients.AMOUNT +
                " FROM " + RecipesIngredients.TABLENAME +
                " JOIN " + Ingredients.TABLENAME +
                " ON " + Ingredients.TABLENAME + "." + Ingredients.ID + " = " + RecipesIngredients.TABLENAME + "." + RecipesIngredients.INGREDIENT_ID +
                " WHERE " + RecipesIngredients.TABLENAME + "." + RecipesIngredients.RECIPE_ID + " = " + id;
        return getReadableDatabase().rawQuery(query, null);
    }

    public void addRecipeData(int recipe_id, int views, int likes, int dislikes) {
        getWritableDatabase().delete(Views.TABLENAME, Views.RECIPE_ID + " = " + recipe_id, null);
        getWritableDatabase().delete(Marks.TABLENAME, Marks.RECIPE_ID + " = " + recipe_id, null);
        ContentValues rViews = new ContentValues();
        ContentValues rMarks = new ContentValues();
        rViews.put(Views.RECIPE_ID, recipe_id);
        rViews.put(Views.VIEWS, views);
        getWritableDatabase().insert(Views.TABLENAME, null, rViews);
        rMarks.put(Marks.RECIPE_ID, recipe_id);
        rMarks.put(Marks.LIKES, likes);
        rMarks.put(Marks.DISLIKES, dislikes);
        getWritableDatabase().insert(Marks.TABLENAME, null, rMarks);
    }

    public void addView(String user_id, String recipe_id) throws SQLiteConstraintException {
        ContentValues content = new ContentValues();
        content.put(UserViews.USER_ID, user_id);
        content.put(UserViews.RECIPE_ID, recipe_id);
        getWritableDatabase().insert(UserViews.TABLENAME, null, content);
    }


    public long addMark(String user_id, String recipe_id, String mark) throws SQLiteConstraintException {
        ContentValues content = new ContentValues();
        content.put(UserMarks.USER_ID, user_id);
        content.put(UserMarks.RECIPE_ID, recipe_id);
        content.put(UserMarks.MARK, mark);
        return getWritableDatabase().insert(UserMarks.TABLENAME, null, content);
    }

    public void removeUserMarks(String user_id) {
        String query = "DELETE FROM " + UserMarks.TABLENAME + " WHERE " + UserMarks.USER_ID + "=" + user_id;
        getWritableDatabase().rawQuery(query, null);
    }

    public int removeMark(String user_id, int recipe_id) {
        try {
            int c = this.getWritableDatabase().delete(UserMarks.TABLENAME, UserMarks.RECIPE_ID + " = " + recipe_id + " and " + UserMarks.USER_ID + " = " + user_id, null);
            return c;
        } catch (Exception e) {
        }
        return -1;
    }

    public Cursor getRecipes(ArrayList<Integer> ingredients) {
        String query = "SELECT DISTINCT " +
                Recipes.TABLENAME + "." + Recipes.ID + ", " +
                Recipes.TABLENAME + "." + Recipes.NAME + ", " +
                Recipes.TABLENAME + "." + Recipes.INSTRUCTIONS + ", " +
                Recipes.TABLENAME + "." + Recipes.PHOTO + ", " +
                Recipes.TABLENAME + "." + Recipes.DURATION + ", " +
                Recipes.TABLENAME + "." + Recipes.YIELD + ", " +
                Recipes.TABLENAME + "." + Recipes.NUTRITION + ", " +
                Recipes.TABLENAME + "." + Recipes.AUTHOR +
                " FROM " + Recipes.TABLENAME +
                " JOIN " + RecipesIngredients.TABLENAME +
                " ON " + Recipes.TABLENAME + "." + Recipes.ID + " = " + RecipesIngredients.TABLENAME + "." + RecipesIngredients.RECIPE_ID;
        boolean first = true;
        for (Integer id : ingredients) {
            if (first) {
                query += " WHERE " + RecipesIngredients.TABLENAME + "." + RecipesIngredients.INGREDIENT_ID + " = " + id;
                first = false;
            } else {
                query += " OR " + RecipesIngredients.TABLENAME + "." + RecipesIngredients.INGREDIENT_ID + " = " + id;
            }

        }
        query += " LIMIT 150";
        return getReadableDatabase().rawQuery(query, null);
    }


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void addRecipeData(ArrayList<ArrayList<Integer>> recipesDataList) {
    }


    public final static class Ingredients {
        private Ingredients() {
        }

        public static final String TABLENAME = "ingredients";
        public static final String ID = "_id";
        public static final String NAME = "name";
    }

    public final static class Recipes {
        private Recipes() {
        }

        public static final String TABLENAME = "recipes";
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String INSTRUCTIONS = "instructions";
        public static final String PHOTO = "photo";
        public static final String DURATION = "duration";
        public static final String YIELD = "yield";
        public static final String NUTRITION = "nutrition";
        public static final String AUTHOR = "author";
    }

    public final static class RecipesIngredients {
        private RecipesIngredients() {
        }

        public static final String TABLENAME = "recipes_ingredients";
        public static final String RECIPE_ID = "recipe_id";
        public static final String INGREDIENT_ID = "ingredient_id";
        public static final String MEASURE = "measure";
        public static final String AMOUNT = "amount";
    }

    public final static class UserViews {
        private UserViews() {
        }

        public final static String TABLENAME = "user_views";
        public final static String USER_ID = "user_id";
        public final static String RECIPE_ID = "recipe_id";

    }

    public final static class Views {
        private Views() {
        }

        public final static String TABLENAME = "views";
        public final static String RECIPE_ID = "recipe_id";
        public final static String VIEWS = "views";
    }

    public final static class UserMarks {
        private UserMarks() {
        }

        public final static String TABLENAME = "user_marks";
        public final static String USER_ID = "user_id";
        public final static String RECIPE_ID = "recipe_id";
        public final static String MARK = "mark";
    }

    public final static class Marks {
        private Marks() {
        }

        public final static String TABLENAME = "marks";
        public final static String RECIPE_ID = "recipe_id";
        public final static String LIKES = "likes";
        public final static String DISLIKES = "dislikes";
    }

    public final static class Recommendations {
        private Recommendations() {
        }

        public final static String TABLENAME = "recommendations";
        public final static String USER_ID = "user_id";
        public final static String RECIPE_ID = "recipe_id";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Ingredients.TABLENAME + " (" +
                Ingredients.ID + " INTEGER NOT NULL, " +
                Ingredients.NAME + " TEXT NOT NULL" +
                ");");
        db.execSQL("CREATE TABLE " + Recipes.TABLENAME + " (" +
                Recipes.ID + " INTEGER NOT NULL, " +
                Recipes.NAME + " TEXT NOT NULL, " +
                Recipes.INSTRUCTIONS + " TEXT NOT NULL, " +
                Recipes.PHOTO + " TEXT, " +
                Recipes.DURATION + " TEXT, " +
                Recipes.YIELD + " TEXT, " +
                Recipes.NUTRITION + " TEXT, " +
                Recipes.AUTHOR + " TEXT" +
                ");");
        db.execSQL("CREATE TABLE " + RecipesIngredients.TABLENAME + " (" +
                RecipesIngredients.RECIPE_ID + " INTEGER NOT NULL, " +
                RecipesIngredients.INGREDIENT_ID + " INTEGER NOT NULL, " +
                RecipesIngredients.MEASURE + " TEXT, " +
                RecipesIngredients.AMOUNT + " TEXT NOT NULL" +
                ");");
        db.execSQL("CREATE TABLE " + Views.TABLENAME + " (" +
                Views.RECIPE_ID + " INTEGER NOT NULL, " +
                Views.VIEWS + " INTEGER NOT NULL DEFAULT 0" +
                ");");
        db.execSQL("CREATE TABLE " + UserMarks.TABLENAME + " (" +
                UserMarks.USER_ID + " INTEGER NOT NULL, " +
                UserMarks.RECIPE_ID + " INTEGER NOT NULL, " +
                UserMarks.MARK + " INTEGER NOT NULL," +
                "PRIMARY KEY(" + UserMarks.USER_ID + "," + UserMarks.RECIPE_ID + ")" +
                ");");
        db.execSQL("CREATE TABLE " + UserViews.TABLENAME + " (" +
                UserViews.USER_ID + " INTEGER NOT NULL," +
                UserViews.RECIPE_ID + " INTEGER NOT NULL," +
                "PRIMARY KEY(" + UserViews.USER_ID + "," + UserViews.RECIPE_ID + ")" +
                ");");
        db.execSQL("CREATE TABLE " + Marks.TABLENAME + " (" +
                Marks.RECIPE_ID + " INTEGER NOT NULL, " +
                Marks.LIKES + " INTEGER NOT NULL DEFAULT 0," +
                Marks.DISLIKES + " INTEGER NOT NULL DEFAULT 0" +
                ");");
        db.execSQL("CREATE TABLE " + Recommendations.TABLENAME + " (" +
                Recommendations.USER_ID + " INTEGER NOT NULL," +
                Recommendations.RECIPE_ID + " INTEGER NOT NULL," +
                "PRIMARY KEY (" + Recommendations.USER_ID + "," + Recommendations.RECIPE_ID + ")" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
