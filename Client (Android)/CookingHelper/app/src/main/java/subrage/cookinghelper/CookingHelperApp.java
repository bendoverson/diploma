package subrage.cookinghelper;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;


/**
 * Created by yung mulatto on 02.04.2017.
 */

public class CookingHelperApp extends Application {
    private static Service service;
    private User user;

    public static SharedPreferences getPreferences() {
        return new CookingHelperApp().getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
    }

    public void onCreate() {
        super.onCreate();
        service = new Service(this);
        SharedPreferences preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
        user = new User(
                preferences.getString(Preferences.ID, null),
                preferences.getString(Preferences.EMAIL, null),
                preferences.getString(Preferences.USERNAME, null),
                preferences.getString(Preferences.PASSWORD, null),
                preferences.getString(Preferences.PASSWORDDEC, null),
                preferences.getString(Preferences.LAST_LOGIN, null));
    }

    public User getUser() {
        service = new Service(this);
        SharedPreferences preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
        return new User(
                preferences.getString(Preferences.ID, null),
                preferences.getString(Preferences.EMAIL, null),
                preferences.getString(Preferences.USERNAME, null),
                preferences.getString(Preferences.PASSWORD, null),
                preferences.getString(Preferences.PASSWORDDEC, null),
                preferences.getString(Preferences.LAST_LOGIN, null));
    }

    public void setUser(User user) {
        this.user = user;
        if (user == null) {
            SharedPreferences preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
            preferences.edit().putString(Preferences.ID, null).apply();
            preferences.edit().putString(Preferences.EMAIL, null).apply();
            preferences.edit().putString(Preferences.USERNAME, null).apply();
            preferences.edit().putString(Preferences.PASSWORD, null).apply();
            preferences.edit().putString(Preferences.PASSWORDDEC, null).apply();
            preferences.edit().putString(Preferences.LAST_LOGIN, null).apply();
        }
    }

    public void setUser(String id, String email, String username, String password, String password_dec, String last_login) {
        this.user = new User(id, email, username, password, password_dec, last_login);
        SharedPreferences preferences = this.getSharedPreferences(String.valueOf(R.string.preferences), Context.MODE_PRIVATE);
        preferences.edit().putString(Preferences.ID, id).apply();
        preferences.edit().putString(Preferences.EMAIL, email).apply();
        preferences.edit().putString(Preferences.USERNAME, username).apply();
        preferences.edit().putString(Preferences.PASSWORD, password).apply();
        preferences.edit().putString(Preferences.PASSWORDDEC, password_dec).apply();
        preferences.edit().putString(Preferences.LAST_LOGIN, last_login).apply();
    }

    public static class User {
        private User(String id, String email, String username, String password, String password_dec, String last_login) {
            this.id = id;
            this.email = email;
            this.username = username;
            this.password = password;
            this.password_dec = password_dec;
            this.last_login = last_login;
        }

        private String id;
        private String email;
        private String username;
        private String password;
        private String password_dec;
        private String last_login;

        public String getId() {
            return this.id;
        }

        public String getEmail() {
            return this.email;
        }

        public String getUsername() {
            return this.username;
        }

        public String getPassword() {
            return this.password;
        }

        public String getPasswordDec() {
            return this.password_dec;
        }

        public String getLastLogin() {
            return this.last_login;
        }

    }

    public static class Preferences {
        private Preferences() {
        }

        public static String LOGGED_IN = "logged_in";
        public static String ID = "id";
        public static String EMAIL = "email";
        public static String USERNAME = "username";
        public static String PASSWORD = "password";
        public static String PASSWORDDEC = "password_dec";
        public static String LAST_LOGIN = "last_login";
        public static String TOKEN = "token";
        public static String SKIPLOGIN = "skip_login";
        public static String RECIPES_DOWNLOADED = "recipes_downloaded";
    }

    public class Service {
        private CookingHelperAPI api;
        private Retrofit retrofit;
        private String BASE_URL = "http://192.168.1.42/cookinghelper/";
        private Gson gson;
        public Database database;
        private OkHttpClient client;

        private Service(CookingHelperApp app) {
            CookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
            client = new OkHttpClient.Builder().cookieJar(cookieJar).build();
            database = new Database(app.getApplicationContext());
            gson = new GsonBuilder().setLenient().create();
            this.retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .callbackExecutor(Executors.newSingleThreadExecutor())
                    .build();
            this.api = retrofit.create(CookingHelperAPI.class);
        }

        public void getUserData() {
            Call<ResponseBody> call = api.getUserData();
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            if (getUser() != null && getUser().getId() != null) {
                                database.removeUserMarks(getUser().getId());
                                JsonParser parser = new JsonParser();
                                JsonArray jsonArray = (JsonArray) parser.parse(response.body().string());
                                JsonArray marks = jsonArray.get(0).getAsJsonArray();
                                JsonArray views = jsonArray.get(1).getAsJsonArray();
                                for (JsonElement _mark : marks) {
                                    JsonObject obj = _mark.getAsJsonObject();
                                    String recipe_id = String.valueOf(obj.get("recipe_id"));
                                    recipe_id = recipe_id.substring(1, recipe_id.length() - 1);
                                    String mark = String.valueOf(obj.get("mark"));
                                    mark = mark.substring(1, mark.length() - 1);
                                    database.addMark(getUser().getId(), recipe_id, mark);
                                }
                                for (JsonElement view : views) {
                                    JsonObject obj = view.getAsJsonObject();
                                    String recipe_id = String.valueOf(obj.get("recipe_id"));
                                    recipe_id = recipe_id.substring(1, recipe_id.length() - 1);
                                    database.addView(getUser().getId(), recipe_id);
                                }
                            }
                        } catch (Exception ignore) {
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

        public CookingHelperAPI getApi() {
            return this.api;
        }
    }

    public Service getService() {
        return this.service;
    }

}
