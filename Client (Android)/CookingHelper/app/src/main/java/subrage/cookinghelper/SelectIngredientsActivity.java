package subrage.cookinghelper;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by interstellar on 16.10.2016.
 */
public class SelectIngredientsActivity extends AppCompatActivity {
    Intent intent;
    private ListView ingredientsList;
    private Database db;
    private IngredientsListAdapter ingredientsAdapter;
    private FilteredIngredientsListAdapter filtederListAdapter;
    private AutoCompleteTextView autoComplete;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_ingredients_activity);
        ingredientsList = (ListView) findViewById(R.id.ingredientsList);
        db = new Database(SelectIngredientsActivity.this);
        autoComplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteIngredients);
        filtederListAdapter = new FilteredIngredientsListAdapter(SelectIngredientsActivity.this);
        ingredientsList.setAdapter(filtederListAdapter);
        class GetIngredientsTask extends AsyncTask<Void, Integer, ArrayList<Ingredient>> {
            @Override
            protected ArrayList<Ingredient> doInBackground(Void... params) {
                Cursor cursor = db.getAllIngredients();
                ArrayList<Ingredient> result = new ArrayList<>();
                int i = 0;
                while (cursor.moveToNext()) {
                    result.add(new Ingredient(cursor.getInt(0), cursor.getString(1)));
                }
                return result;
            }

            protected void onProgressUpdate(Integer... progress) {
            }

            protected void onPostExecute(ArrayList<Ingredient> result) {
                super.onPostExecute(result);
                ingredientsAdapter = new IngredientsListAdapter(SelectIngredientsActivity.this, result);
                autoComplete.setAdapter(ingredientsAdapter);
                autoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        autoComplete.setText("");
                        Ingredient ingredient = ingredientsAdapter.getItem(position);
                        if (!filtederListAdapter.contains(ingredient)) {
                            filtederListAdapter.add(ingredient);
                            filtederListAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(SelectIngredientsActivity.this, "Ингредиент \"" + ingredient.getName() + "\" уже добавлен", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
        new GetIngredientsTask().execute();
    //    setTitle(R.string.select_ingredients);
    }

    public void onSearchRecipesClick(View view) {
        if (filtederListAdapter.getIngredients().size() == 0) {
            Toast.makeText(SelectIngredientsActivity.this, "Список ингредиентов пуст", Toast.LENGTH_SHORT).show();
        } else {
            intent = new Intent(this, ResultRecipesActivity.class);
            intent.putParcelableArrayListExtra("Ingredients", filtederListAdapter.getIngredients());
            startActivity(intent);
        }
    }

}
