package subrage.cookinghelper;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by yung mulatto on 02.04.2017.
 */

public interface CookingHelperAPI {
    @GET("api/getUserRecommendedRecipes")
    Call<ResponseBody> getUserRecommendedRecipes();

    @GET("api/getRecipesIngredients")
    Call<ResponseBody> getRecipesIngredients();

    @FormUrlEncoded
    @POST("api/getRecipesData")
    Call<ResponseBody> getRecipesData(@Field("recipes[]") ArrayList<Integer> list);


    @GET("api/getUserData")
    Call<ResponseBody> getUserData();

    @FormUrlEncoded
    @POST("api/addView")
    Call<ResponseBody> addView(@Field("recipe_id") String recipe_id);

    @FormUrlEncoded
    @POST("api/manageMark")
    Call<ResponseBody> manageMark(@Field("recipe_id") String recipe_id, @Field("mark") String mark);

    @GET("oauth/logout")
    Call<ResponseBody> logout();

    @GET("oauth/loggedin")
    Call<ResponseBody> isLoggedIn();

    @FormUrlEncoded
    @POST("oauth/login")
    Call<ResponseBody> logIn(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("oauth/register")
    Call<ResponseBody> registerUser(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirm") String password_confirm
    );
}
