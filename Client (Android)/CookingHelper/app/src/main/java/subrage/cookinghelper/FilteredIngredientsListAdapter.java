package subrage.cookinghelper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by interstellar on 04.03.2017.
 */
public class FilteredIngredientsListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Ingredient> ingredients;
    private LayoutInflater inflater;
    FilteredIngredientsListAdapter(Context context){
        this.context = context;
        ingredients  = new ArrayList<>();
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public boolean contains(Ingredient obj){
        if(ingredients.contains(obj)) return true;
        return false;
    }
    @Override
    public int getCount() {
        return ingredients.size();
    }
    public void add(Ingredient obj){
        this.ingredients.add(obj);
    }
    public void remove(Ingredient obj){
        this.ingredients.remove(obj);
    }
    public ArrayList<Ingredient> getIngredients(){
        return this.ingredients;
    }
    @Override
    public Object getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ingredients.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        this.notifyDataSetChanged();
        View view = convertView;
        if(view == null){
            view = inflater.inflate(R.layout.ingredient, parent, false);
        }
        final Ingredient ingredient = (Ingredient) getItem(position);
        ImageButton btn = (ImageButton) view.findViewById(R.id.removeIngredient);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(ingredient);
                Toast.makeText(context, "Ингредиент \""+ingredient.getName()+"\" удален", Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
            }
        });

        TextView field = (TextView) view.findViewById(R.id.ingredientName);
        field.setText(ingredient.getName());
        return view;
    }
}
