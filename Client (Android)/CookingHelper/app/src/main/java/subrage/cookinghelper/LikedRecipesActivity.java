package subrage.cookinghelper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by interstellar on 23.04.2017.
 */

public class LikedRecipesActivity extends AppCompatActivity {
    private ListView recipesList;
    private RecipesListAdapter recipesListAdapter;
    private Database database;
    private LinearLayout imageLayout;
    private TextView notFoundMessage;
    private Intent newIntent;
    ArrayList<Recipe> resultRecipes;
    private HashMap<Integer, Integer> userMarks;
    private CookingHelperApp app;
    private SharedPreferences preferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_recipes_activity);
        app = (CookingHelperApp) getApplication();
        preferences = app.getSharedPreferences(String.valueOf(R.string.preferences), MODE_PRIVATE);
        final boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
        recipesList = (ListView) findViewById(R.id.resultRecipesList);
        imageLayout = (LinearLayout) findViewById(R.id.imageLayout);
        GifImageView gifImageView = (GifImageView) findViewById(R.id.loading);
        gifImageView.setGifImageResource(R.drawable.ringalt3);
        notFoundMessage = (TextView) findViewById(R.id.notFound);
        database = new Database(this);
        userMarks = new HashMap<>();
        resultRecipes = new ArrayList<>();
        final ArrayList<ArrayList<Integer>> recipesDataList = new ArrayList<>();
        class GetRecipes extends AsyncTask<Void, Void, ArrayList<Recipe>> {
            @Override
            protected ArrayList<Recipe> doInBackground(Void... params) {
                ArrayList<String[]> likes = new ArrayList<>();
                String user_id = preferences.getString(CookingHelperApp.Preferences.ID, null);
                Cursor likesCursor = database.getUserLikes(user_id);
                ArrayList<Integer> recipesIds = new ArrayList<>();
                while (likesCursor.moveToNext()) {
                    String recipe = likesCursor.getString(0);
                    String mark = likesCursor.getString(1);
                    recipesIds.add(likesCursor.getInt(0));
                    userMarks.put(Integer.parseInt(recipe), Integer.parseInt(mark));
                    likes.add(new String[] {recipe, mark});
                }
                for (final String[] like : likes) {
                    Cursor recipeCursor = database.getRecipe(like[0]);
                    final Recipe recipe;
                    recipeCursor.moveToNext();
                    recipe = new Recipe(
                            recipeCursor.getInt(0),
                            recipeCursor.getString(1),
                            recipeCursor.getString(2),
                            recipeCursor.getString(3),
                            recipeCursor.getString(4),
                            recipeCursor.getString(5),
                            recipeCursor.getString(6),
                            recipeCursor.getString(7),
                            null,
                            null
                    );

                    ArrayList<Ingredient> recipeIngredients = new ArrayList<>();
                    Cursor ingredientsCursor = database.getRecipeIngredients(Integer.parseInt(like[0]));

                    while (ingredientsCursor.moveToNext()) {
                        int _id = ingredientsCursor.getInt(0);
                        String _name = ingredientsCursor.getString(1);
                        String _measure = ingredientsCursor.getString(2);
                        String _amount = ingredientsCursor.getString(3);
                        recipeIngredients.add(new Ingredient(_id, _name, _measure, _amount));
                    }
                    recipe.setIngredients(recipeIngredients);
                    recipe.setMissingIngredients(new ArrayList<Ingredient>());
                    resultRecipes.add(recipe);
                    final Call<ResponseBody> recipesData = app.getService().getApi().getRecipesData(recipesIds);
                    recipesData.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            JsonParser parser = new JsonParser();
                            try {
                                if (response.isSuccessful()) {
                                    JsonObject array = parser.parse(response.body().string()).getAsJsonObject();
                                    for (final Recipe recipe : resultRecipes) {
                                        final JsonArray recipeArray = array.get(recipe.getId() + "").getAsJsonArray();
                                        recipe.setViews(recipeArray.get(0).getAsInt());
                                        recipe.setLikes(recipeArray.get(1).getAsInt());
                                        recipe.setDisliked(recipeArray.get(2).getAsInt());
                                        ArrayList<Integer> temp = new ArrayList<Integer>();
                                        temp.add(recipe.getId());
                                        temp.add(recipeArray.get(0).getAsInt());
                                        temp.add(recipeArray.get(1).getAsInt());
                                        temp.add(recipeArray.get(2).getAsInt());
                                        recipesDataList.add(temp);
                                    }
                                    class addRecipeData extends AsyncTask<Void, Void, Void> {
                                        @Override
                                        protected Void doInBackground(Void... voids) {
                                            database.addRecipeData(recipesDataList);
                                            return null;
                                        }
                                    }
                                    new addRecipeData().execute();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imageLayout.setVisibility(View.GONE);
                                            if (resultRecipes.isEmpty()) {
                                                notFoundMessage.setVisibility(View.VISIBLE);
                                            } else {
                                                recipesList.setVisibility(View.VISIBLE);
                                                recipesListAdapter = new RecipesListAdapter(LikedRecipesActivity.this, resultRecipes, userMarks);

                                                recipesList.setAdapter(recipesListAdapter);
                                                recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                        Recipe recipe = (Recipe) recipesListAdapter.getItem(position);
                                                        newIntent = new Intent(LikedRecipesActivity.this, RecipeActivity.class);
                                                        newIntent.putExtra("Recipe", recipe);
                                                        if (userMarks.containsKey(recipe.getId())) {
                                                            int mark = userMarks.get(recipe.getId());
                                                            newIntent.putExtra("Mark", mark);
                                                        }
                                                        newIntent.putExtra("Position", position);
                                                        startActivityForResult(newIntent, 0);
                                                    }
                                                });
                                            }
                                        }
                                    });

                                } else {
                                    Log.d("CODE", response.code() + "");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            for (Recipe recipe : resultRecipes) {
                                Cursor rViews = database.getRecipeViews(recipe.getId());
                                if (rViews.moveToNext()) {
                                    recipe.setViews(rViews.getInt(1));
                                }
                                Cursor rMarks = database.getRecipeMarks(recipe.getId());
                                if (rMarks.moveToNext()) {
                                    recipe.setLikes(rMarks.getInt(1));
                                    recipe.setDisliked(rMarks.getInt(2));
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        imageLayout.setVisibility(View.GONE);
                                        if (resultRecipes.isEmpty()) {
                                            notFoundMessage.setVisibility(View.VISIBLE);
                                        } else {
                                            recipesList.setVisibility(View.VISIBLE);
                                            recipesListAdapter = new RecipesListAdapter(LikedRecipesActivity.this, resultRecipes, userMarks);
                                            recipesList.setAdapter(recipesListAdapter);
                                            recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    Recipe recipe = (Recipe) recipesListAdapter.getItem(position);
                                                    newIntent = new Intent(LikedRecipesActivity.this, RecipeActivity.class);
                                                    newIntent.putExtra("Recipe", recipe);
                                                    if (userMarks.containsKey(recipe.getId())) {
                                                        int mark = userMarks.get(recipe.getId());
                                                        newIntent.putExtra("Mark", mark);
                                                    }
                                                    newIntent.putExtra("Position", position);
                                                    startActivityForResult(newIntent, 0);
                                                }
                                            });
                                        }
                                    }
                                });

                            }
                        }
                    });
                }
                return resultRecipes;
            }
        }

        String user_id = preferences.getString(CookingHelperApp.Preferences.ID, null);
        if (logged_in || user_id == null) {
            new GetRecipes().execute();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notFoundMessage.setText(R.string.notLoggedIn);
                    notFoundMessage.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            int position = data.getIntExtra("Position", -1);
            if (position != -1) {
                Recipe recipe = (Recipe) recipesListAdapter.getItem(position);
                switch (resultCode) {
                    case 0:
                        recipesListAdapter.removeMarkedRecipe(recipe.getId());
                        break;
                    default:
                        recipesListAdapter.addMarkedRecipe(recipe.getId(), resultCode);
                        break;
                }
                recipesListAdapter.notifyDataSetChanged();

            }
        }
    }
}
