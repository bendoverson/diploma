package subrage.cookinghelper;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RecipeActivity extends AppCompatActivity {
    Intent intent;
    LinearLayout ingredients;
    TextView instructions;
    ImageView photo;
    Database database;
    Recipe recipe;
    Button likeButton;
    Button dislikeButton;
    Intent resultIntent;
    TextView duration;
    TextView yield;
    TextView nutrition;
    TextView author;
    ColorStateList disabled;
    ColorStateList liked;
    ColorStateList disliked;

    CookingHelperApp app;
    SharedPreferences preferences;
    int mMark;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_activity);
        duration = (TextView) findViewById(R.id.singleRecipeDuration);
        yield = (TextView) findViewById(R.id.singleRecipeYield);
        nutrition = (TextView) findViewById(R.id.singleRecipeNutrition);
        author = (TextView) findViewById(R.id.singleRecipeAuthor);
        app = (CookingHelperApp) getApplication();
        preferences = app.getSharedPreferences(String.valueOf(R.string.preferences), MODE_APPEND);
        boolean logged_in = preferences.getBoolean(CookingHelperApp.Preferences.LOGGED_IN, false);
        intent = getIntent();
        database = new Database(this);
        disabled = ContextCompat.getColorStateList(this, R.color.disabled);
        liked = ContextCompat.getColorStateList(this, R.color.colorPrimary);
        disliked = ContextCompat.getColorStateList(this, R.color.background4);
        ingredients = (LinearLayout) findViewById(R.id.singleRecipeIngredients);
        instructions = (TextView) findViewById(R.id.singleRecipeInstructions);
        photo = (ImageView) findViewById(R.id.singleRecipePhoto);

        likeButton = (Button) findViewById(R.id.likeButton);
        dislikeButton = (Button) findViewById(R.id.dislikeButton);
        if (!logged_in) {
            likeButton.setVisibility(View.GONE);
            dislikeButton.setVisibility(View.GONE);
        }
        likeButton.setBackgroundTintList(disabled);
        dislikeButton.setBackgroundTintList(disabled);
        resultIntent = new Intent(RecipeActivity.this, ResultRecipesActivity.class);
        resultIntent.putExtra("Position", intent.getIntExtra("Position", -1));
        recipe = intent.getParcelableExtra("Recipe");
        duration.setText(recipe.getDuration());
        nutrition.setText(recipe.getNutrition());
        yield.setText(recipe.getYield());
        author.setText(recipe.getAuthor());
        Call<ResponseBody> addView = app.getService().getApi().addView(String.valueOf(recipe.getId()));
        addView.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
        Resources resources = this.getResources();
        int resID = resources.getIdentifier(recipe.getPhotoSrc(), "drawable", this.getPackageName());
        Log.d("resID", resID + "");
        try {
            Drawable drawable = resources.getDrawable(resID, this.getTheme());
            photo.setImageDrawable(drawable);
        } catch (Exception ignored) {

        }
        mMark = intent.getIntExtra("Mark", 0);
        Log.d("isMarked", mMark + "");
        switch (mMark) {
            case -1:
                dislikeButton.setBackgroundTintList(disliked);
                break;
            case 1:
                likeButton.setBackgroundTintList(liked);
                break;
        }
        setTitle(recipe.getName());

        class AddView extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                CookingHelperApp.User user = app.getUser();
                if (user != null) {
                    database.addView(user.getId(), recipe.getId() + "");
                }
                return null;
            }

        }
        new AddView().execute();

        for (Ingredient ingredient : recipe.getIngredients()) {
            LinearLayout layout = new LinearLayout(this);
            layout.setPadding(0, 5, 0, 0);
            layout.setBackgroundColor(getResources().getColor(R.color.black, null));
            TextView name = new TextView(this), amount = new TextView(this), measure = new TextView(this);
            String ingName = ingredient.getName().substring(0, 1).toUpperCase() + ingredient.getName().substring(1);
            name.setText(ingName);
            amount.setText(ingredient.getAmount());
            measure.setText(ingredient.getMeasure());
            name.setBackgroundColor(getResources().getColor(R.color.background2, null));
            amount.setBackgroundColor(getResources().getColor(R.color.background2, null));
            measure.setBackgroundColor(getResources().getColor(R.color.background2, null));
            name.setPadding(30, 10, 10, 10);
            amount.setPadding(10, 10, 10, 10);
            measure.setPadding(10, 10, 30, 10);
            name.setTypeface(Typeface.create("sans-serif-condensed", Typeface.NORMAL));
            amount.setTypeface(Typeface.create("sans-serif-condensed", Typeface.NORMAL));
            measure.setTypeface(Typeface.create("sans-serif-condensed", Typeface.NORMAL));
            name.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 20));
            layout.addView(name);
            if (ingredient.getMeasure() == null || Objects.equals(ingredient.getMeasure(), "")) {
                amount.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
                layout.addView(amount);
            } else {
                amount.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, (float) 1));
                measure.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, (float) 1));
                layout.addView(amount);
                layout.addView(measure);
            }
            ingredients.addView(layout);
        }
        instructions.setText(recipe.getInstructions());
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void onLikeButtonClick(View view) {
        if (mMark == 1) {
            mMark = 0;
        } else {
            mMark = 1;
        }
        Call<ResponseBody> manageMark = app.getService().getApi().manageMark(String.valueOf(recipe.getId()), String.valueOf(mMark));
        manageMark.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body().string() != "0") {
                            int rMark = -2;
                            long addMark = -2;
                            switch (mMark) {
                                case 1:
                                    database.removeMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId());
                                    addMark = database.addMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId() + "", mMark + "");
                                    if (addMark > 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dislikeButton.setBackgroundTintList(disabled);
                                                likeButton.setBackgroundTintList(liked);
                                            }
                                        });
                                        setResult(1, resultIntent);
                                    }
                                    break;
                                case 0:
                                    rMark = database.removeMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId());
                                    if (rMark > 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dislikeButton.setBackgroundTintList(disabled);
                                                likeButton.setBackgroundTintList(disabled);
                                            }
                                        });
                                        setResult(0, resultIntent);
                                    }
                                    break;
                            }
                        } else {
                            final String message = response.message() + " " + response.code();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                final String message = t.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void onDislikeButtonClick(View view) {
        if (mMark == -1) {
            mMark = 0;
        } else {
            mMark = -1;
        }
        Call<ResponseBody> manageMark = app.getService().getApi().manageMark(String.valueOf(recipe.getId()), String.valueOf(mMark));
        manageMark.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body().string() != "0") {
                            int rMark = -2;
                            long addMark = -2;
                            switch (mMark) {
                                case -1:
                                    database.removeMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId());
                                    addMark = database.addMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId() + "", mMark + "" +
                                            "");
                                    if (addMark > 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dislikeButton.setBackgroundTintList(disliked);
                                                likeButton.setBackgroundTintList(disabled);
                                            }
                                        });
                                        setResult(-1, resultIntent);
                                    }
                                    break;
                                case 0:
                                    rMark = database.removeMark(preferences.getString(CookingHelperApp.Preferences.ID, null), recipe.getId());
                                    if (rMark > 0) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                dislikeButton.setBackgroundTintList(disabled);
                                                likeButton.setBackgroundTintList(disabled);
                                            }
                                        });
                                        setResult(0, resultIntent);
                                    }
                                    break;
                            }
                        } else {
                            final String message = response.message() + " " + response.code();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, final Throwable t) {
                final String message = t.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }


}
